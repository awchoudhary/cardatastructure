import java.io.Serializable;

/**
 * The following class creates a CarStatus object that records a cars position, time, speed and the road
 * that it is travelling on.
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
 */
public class CarStatus implements Serializable {
	private int id;
	private String road;
	private double position;
	private double time;
	private double speed = 0;
	
	/**
	 * A constructor for CarStatus that creates an instance of the object with default values for road, time, position and
	 * id.
	 * @param newId
	 * @param newRoad
	 * @param newPosition
	 * @param newTime
	 */
	public CarStatus(int newId, String newRoad, double newPosition, double newTime){
		id = newId;
		road = newRoad;
		position = newPosition;
		time = newTime;
	}
	
	/**
	 * Accessor for id variable
	 * @return id
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Mutator for id variable
	 * @param newId
	 */
	public void setId(int newId){
		id = newId;
	}
	
	/**
	 * Accessor for road String
	 * @return road
	 */
	public String getRoad(){
		return road;
	}
	
	/**
	 * Mutator for road String
	 * @param newRoad
	 */
	public void setRoad(String newRoad){
		road = newRoad;
	}
	
	/**
	 * Accessor for position variable
	 * @return position
	 */
	public double getPosition(){
		return position;
	}
	
	/**
	 * Mutator for position variable
	 * @param newPosition
	 */
	public void setPosition(double newPosition){
		position = newPosition;
	}
	
	/**
	 * Accessor for time variable
	 * @return time
	 */
	public double getTime(){
		return time;
	}
	
	/**
	 * Mutator for time variable
	 * @param newTime
	 */
	public void setTime(double newTime){
		time = newTime;
	}
	
	/**
	 * Accessor for speed variable
	 * @return speed
	 */
	public double getSpeed(){
		return speed;
	}
	
	/**
	 * Mutator for speed variable
	 * @param newSpeed
	 */
	public void setSpeed(double newSpeed){
		speed = newSpeed;
	}
	
}

