import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;

/**
 * The following class implemets two hash table data structures. One which maps a car id to the CarStatus and the other which maps
 * a road to a list of cars traveling on that road. The list being used is an int ArrayList. 
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
 */
public class TrafficMonitor implements Serializable {
	private Hashtable<Integer, CarStatus> cars = new Hashtable<Integer, CarStatus>();
	private Hashtable<String, List<Integer>> roads = new Hashtable<String, List<Integer>>();
	
	/**
	 * Creates an instance of the TrafficMonitor class and takes no arguments. 
	 */
	public TrafficMonitor(){
		cars = new Hashtable<Integer, CarStatus>();
		roads = new Hashtable<String, List<Integer>>();
	}
	
	/**
	 * Is the accessor method for the cars hashtable.
	 * @return cars
	 */
	public Hashtable<Integer, CarStatus> getCars(){
		return cars;
	}
	
	/**
	 * Mutator method for the cars hashtable.
	 * @param newCars
	 */
	public void setCars(Hashtable<Integer, CarStatus> newCars){
		cars = newCars;
	}
	
	/**
	 * Accessor method for the roads hashtable.
	 * @return roads
	 */
	public Hashtable<String, List<Integer>> getRoads(){
		return roads;
	}
	
	/**
	 * Mutator method for the roads hashtable
	 * @param newRoads
	 */
	public void setRoads(Hashtable<String, List<Integer>> newRoads){
		roads = newRoads;
	}
	
	/**
	 * Updates both hashtables with new information for the car is the car exists, or creates a new CarStatus object and adds the
	 * car to both hashtables accordingly. 
	 * @param id
	 * @param road
	 * @param position
	 * @param time
	 */
	public void addData(int id, String road, double position, double time){
		double newSpeed;
		if(cars.get(id) != null){
			if(!(road.equals(cars.get(id).getRoad()))){
				cars.get(id).setSpeed(0);
				if(roads.containsKey(road)){
					roads.get(cars.get(id).getRoad()).remove(new Integer(id));
					roads.get(road).add(id);
				}
				else{
					List<Integer> a = new ArrayList<Integer>();
					roads.put(road, a);
					roads.get(road).add(id);
					roads.get(cars.get(id).getRoad()).remove(new Integer(id));
				}
			}
			else{
				newSpeed = (position - cars.get(id).getPosition()) / (time - cars.get(id).getTime());
				cars.get(id).setSpeed(newSpeed);
			}
			
			cars.get(id).setRoad(road);
			cars.get(id).setPosition(position);
			cars.get(id).setTime(time);
		}
		else{
			CarStatus a = new CarStatus(id, road, position, time);		
			cars.put(id, a);
			if(roads.containsKey(road)){
				roads.get(road).add(id);
			}
			else{
				List<Integer> b = new ArrayList<Integer>();
				roads.put(road, b);
				roads.get(road).add(id);
			}
		}
	}
	
	/**
	 * removes a car from both hashtables. 
	 * @param id
	 */
	public void remove(int id){
		if(cars.get(id) != null){
			roads.get(cars.get(id).getRoad()).remove(new Integer(id));
			cars.remove(id);
			
		}
		else{
			System.out.println("Car #" + cars.get(id).getId() + " unknown.");
		}
	}
	
	/**
	 * returns the road on which the car with the given id is travelling on, or returns null if the car does not exist.  
	 * @param id
	 * @return road
	 */
	public String getRoad(int id){
		if(cars.get(id) != null){
			return cars.get(id).getRoad();
		}
		else{
			return null;
		}
	}
	
	/**
	 * Returns the speed of the car with the given id if the speed of that car is known. Returns -1 if the car does not exist
	 * and -2 if the cars speed is unknown. 
	 * @param id
	 * @return speed or -1 or -2
	 */
	public double getSpeed(int id){
		if(cars.get(id) != null){
			if(cars.get(id).getSpeed() == 0){
				return -2;
			}
			else{
				return cars.get(id).getSpeed();
			}
		}
		else{
			return -1;
		}
	}
	 /**
	  * returns an int ArrayList containing the known speeds of all cars on the given road.
	  * @param road
	  * @return List<Double> speeds
	  */
	public List<Double> getKnownSpeeds(String road){
		if(roads.get(road) != null){
			List<Double> speeds = new ArrayList<Double>();
			ListIterator<Integer> litr = roads.get(road).listIterator();
			while(litr.hasNext()){
				double speed = cars.get(litr.next()).getSpeed();
				if(speed != 0){
					speeds.add(speed);
				}
			}
			return speeds;
		}
		else{
			return null;
		}
		
	}
	
	/**
	 * Calculates the average speed of the cars on the specified road with the help of getKnownSpeeds() 
	 * @param road
	 * @return Double average
	 */
	public double getAverageSpeed(String road){
		double total = 0;
		List<Double> speeds = getKnownSpeeds(road);
		for(int i = 0; i < speeds.size(); i++){
			total += speeds.get(i);
		}
		double average = total / speeds.size();
		return average;
	}
	
	/**
	 * Saves the TrafficMonitor into the filename that is specified by the parameter upon exiting the program. 
	 * @param filename
	 */
	public void serialize(String filename){

		try {
			FileOutputStream file = new FileOutputStream(filename);
			ObjectOutputStream fout = new ObjectOutputStream(file);
			fout.writeObject(this);
			fout.close();
		} catch (IOException e){

		}
	}
	
	/**
	 * checks if the specified file exists and loads it upon executing the program.
	 * @param filename
	 * @return myTree
	 * @throws ClassNotFoundException
	 */
	public static TrafficMonitor deserialize(String filename) throws ClassNotFoundException{
		TrafficMonitor myMonitor = null;
		try {
			FileInputStream file = new FileInputStream(filename);
			ObjectInputStream fin  = new ObjectInputStream(file);
			myMonitor = (TrafficMonitor) fin.readObject(); 
			fin.close(); 
			System.out.println("Loaded TrafficMonitor from “traffic.obj”");

		} catch(IOException e){ 
			System.out.println("traffic.obj not found. Using new TrafficMonitor.");
			myMonitor = new TrafficMonitor();
		}
		return myMonitor;

	}
	
	
}
