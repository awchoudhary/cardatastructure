import java.util.List;
import java.util.Scanner;

/**
 * The following class prompts the user with a menu and executes various operations with the help of the CarStatus and TrafficMonitor
 * classes, based on user input. 
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
 */
public class TrafficDriver {
	public static void main(String []args) throws ClassNotFoundException{
		TrafficMonitor monitor = TrafficMonitor.deserialize("traffic.obj");
		for(;;){
			System.out.println("\nA) Add data");
			System.out.println("R) Remove car");
			System.out.println("D) Get road");
			System.out.println("S) Get speed");
			System.out.println("T) Get all speeds on a road");
			System.out.println("U) Get average speed");
			System.out.println("Q) Quit and save to traffic.obj");
			Scanner input = new Scanner(System.in);
			System.out.println("\nEnter a selection: ");
			String option = input.nextLine();
			if(option.equalsIgnoreCase("a")){
				System.out.println("Enter the car ID number: ");
				int id = Integer.parseInt(input.nextLine());
				System.out.println("Enter the road name: ");
				String road = input.nextLine();
				System.out.println("Enter the position: ");
				double position = Double.parseDouble(input.nextLine());
				System.out.println("Enter the time: ");
				double time = Double.parseDouble(input.nextLine());
				monitor.addData(id, road, position, time);
				System.out.print("Added car #" + id + " on " + road + ", position = " + position + ", time = " + time);
				if(monitor.getSpeed(id) > 0){
					System.out.println(", speed = " + monitor.getSpeed(id));
				}
			}
			else if(option.equalsIgnoreCase("r")){
				System.out.println("Enter the car ID number: ");
				int id = Integer.parseInt(input.nextLine());
				monitor.remove(id);
				System.out.println("Removed car #" + id);
			}
			else if(option.equalsIgnoreCase("d")){
				System.out.println("Enter the car ID number: ");
				int id = Integer.parseInt(input.nextLine());
				if(monitor.getRoad(id) != null){
					System.out.println("Car #" + id + " is on " + monitor.getRoad(id));
				}
				else{
					System.out.println("Car #" + id + " unknown.");
				}
			}
			else if(option.equalsIgnoreCase("s")){
				System.out.println("Enter the car ID number: ");
				int id = Integer.parseInt(input.nextLine());
				if(monitor.getSpeed(id) == -1){
					System.out.println("Car #" + id + " unknown.");
				}
				else if(monitor.getSpeed(id) == -2){
					System.out.println("The speed of car #" + id + " is unknown.");
				}
				else{
					System.out.println("The speed of car #" + id + " is " + monitor.getSpeed(id));
				}
			}
			else if(option.equalsIgnoreCase("t")){
				System.out.println("Enter Road name: ");
				String road = input.nextLine();
				List<Double> speeds = monitor.getKnownSpeeds(road);
				for(int i = 0; i < speeds.size(); i++){
					System.out.println("Car #" + monitor.getRoads().get(road).get(i) + ": " + speeds.get(i));
				}
			}
			else if(option.equalsIgnoreCase("u")){
				System.out.println("Enter road name: ");
				String road = input.nextLine();
				double average = monitor.getAverageSpeed(road);
				System.out.println("Average speed of cars on " + road + " is " + average);
			}
			else if(option.equalsIgnoreCase("q")){
				monitor.serialize("traffic.obj");
				System.out.println("Saved TrafficMonitor to “traffic.obj”");
				System.exit(0);
			}
			else{
				System.out.println("Invalid option");
			}
		}
	}
}
